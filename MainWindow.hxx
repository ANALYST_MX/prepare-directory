#pragma once

#include <QtWidgets>
#include <QWidget>
#include <QDir>

class MainWindow : public QWidget
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

protected:
  void view();

private slots:
  void create();
  void browse();
  void find();
  void openDirectoryOfItem(int, int);

private:
  QPushButton *createButton(const QString &, const char *);
  QComboBox *createComboBox(const QString & = QString());
  QLabel *createLabel(const QString & = QString());
  QTableWidget *createTable(int rows, int columns);
  void createDirectoriesTable(const char *);
  void showFiles(const QStringList &);

  QDir currentDir;
  QComboBox *directoryComboBox;
  QLabel *directoryLabel;
  QLabel *directoriesFoundLabel;
  QPushButton *browseButton;
  QPushButton *findButton;
  QPushButton *mkdirsButton;
  QTableWidget *directoriesTable;

private:
  static void updateComboBox(QComboBox *);
};

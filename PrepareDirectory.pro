#-------------------------------------------------
#
# Project created by QtCreator 2015-10-19T12:05:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PrepareDirectory
TEMPLATE = app


SOURCES += main.cxx\
        MainWindow.cxx

HEADERS  += main.hxx\
        MainWindow.hxx

FORMS    +=

CONFIG += c++11

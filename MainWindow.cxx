#include "MainWindow.hxx"

MainWindow::MainWindow(QWidget *parent) :
  QWidget(parent)
{
  view();
}

void MainWindow::view()
{
  mkdirsButton = createButton(tr("&Create"), SLOT(create()));
  browseButton = createButton(tr("&Browse..."), SLOT(browse()));
  findButton = createButton(tr("&Find"), SLOT(find()));

  directoryComboBox = createComboBox(QDir::currentPath());

  directoryLabel = createLabel(tr("In directory:"));
  directoriesFoundLabel = createLabel();

  createDirectoriesTable(SLOT(openDirectoryOfItem(int, int)));

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(directoryLabel, 0, 0);
  mainLayout->addWidget(directoryComboBox, 0, 1, 1, 2);
  mainLayout->addWidget(browseButton, 0, 3);
  mainLayout->addWidget(directoriesTable, 1, 0, 1, 4);
  mainLayout->addWidget(directoriesFoundLabel, 2, 0, 1, 2);
  mainLayout->addWidget(findButton, 2, 2);
  mainLayout->addWidget(mkdirsButton, 2, 3);
  setLayout(mainLayout);

  setWindowTitle(tr("Create Directories"));
  resize(700, 300);
}

MainWindow::~MainWindow()
{
}

QPushButton *MainWindow::createButton(const QString &text, const char *method)
{
  QPushButton *button = new QPushButton(text);
  connect(button, SIGNAL(clicked()), this, method);

  return button;
}

QComboBox *MainWindow::createComboBox(const QString &text)
{
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}

QLabel *MainWindow::createLabel(const QString &text)
{
    QLabel *label = new QLabel(tr(text.toStdString().c_str()));

    return label;
}

QTableWidget *MainWindow::createTable(int rows, int columns)
{
  QTableWidget *table = new QTableWidget(rows, columns);
  table->setSelectionBehavior(QAbstractItemView::SelectRows);
  table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
  table->verticalHeader()->hide();
  table->setShowGrid(false);

  return table;
}

void MainWindow::create()
{
  if (directoryComboBox->currentText().isEmpty())
    {
      return;
    }

  QStringList SMOs;
  SMOs << "АСТРА-МЕТАЛЛ" << "БАШКОРТОСТАН-МЕДИЦИНА" << "МАКС-М"
       << "РГС-МЕДИЦИНА" << "СОГАЗ-МЕД" << "СПАСЕНИЕ"
       << "ТФОМС" << "УРАЛСИБ" << "ЮГОРИЯ-МЕД";

  QDir dir(directoryComboBox->currentText());
  for (auto smo : SMOs)
    {
      dir.mkdir(smo);
    }
}

void MainWindow::browse()
{
  QString directory = QFileDialog::getExistingDirectory(this, tr("Find Dirs"), directoryComboBox->currentText());

  if (!directory.isEmpty()) {
      if (-1 == directoryComboBox->findText(directory))
        {
          directoryComboBox->addItem(directory);
        }
      directoryComboBox->setCurrentIndex(directoryComboBox->findText(directory));
  }
}

void MainWindow::find()
{
  directoriesTable->setRowCount(0);

  QString path = directoryComboBox->currentText();

  updateComboBox(directoryComboBox);

  currentDir = QDir(path);
  QStringList files;
  files = currentDir.entryList(QStringList("*"),
                               QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden,
                               QDir::Name);

  showFiles(files);
}

void MainWindow::showFiles(const QStringList &dirs)
{
    for (auto &dir : dirs) {
        QTableWidgetItem *directoryNameItem = new QTableWidgetItem(dir);
        directoryNameItem->setFlags(directoryNameItem->flags() ^ Qt::ItemIsEditable);

        int row = directoriesTable->rowCount();
        directoriesTable->insertRow(row);
        directoriesTable->setItem(row, 0, directoryNameItem);
    }
    directoriesFoundLabel->setText(tr("%1 dir(s) found").arg(dirs.size()) +
                             (" (Double click on a dir to open it)"));
    directoriesFoundLabel->setWordWrap(true);
}

void MainWindow::openDirectoryOfItem(int row, int /*column*/)
{
  QTableWidgetItem *item = directoriesTable->item(row, 0);

  QDesktopServices::openUrl(QUrl::fromLocalFile(currentDir.absoluteFilePath(item->text())));
}

void MainWindow::createDirectoriesTable(const char *method)
{
  QStringList labels;
  directoriesTable = createTable(0, 1);
  labels << tr("Directory");
  directoriesTable->setHorizontalHeaderLabels(labels);

  connect(directoriesTable, SIGNAL(cellActivated(int, int)), this, method);
}

void MainWindow::updateComboBox(QComboBox *comboBox)
{
  if (-1 == comboBox->findText(comboBox->currentText()))
    {
      comboBox->addItem(comboBox->currentText());
    }
}
